package cst438hw3.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TempAndTime
{
    public double temp;
    public long time;
    public int timezone;

    public TempAndTime(double temp, long time, int timezone)
    {
        this.temp = temp;
        this.time = time;
        this.timezone = timezone;
    }

    /**
     * Convert the temperature to Farenheit.
     *
     * @return
     */
    public double getfahrenheit()
    {
        double temp = (this.temp - 273.15) * 9.0 / 5.0 + 32.0;
        BigDecimal bd = new BigDecimal(temp).setScale(2, RoundingMode.HALF_UP);
        double farenheit = bd.doubleValue();
        return farenheit;
    }

    /**
     * Convert the return time to the local time.
     *
     * @return
     */
    public String getTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        long timestamp = (this.time * 1000) + (timezone * 1000);
        return dateFormat.format(new Date(timestamp));
    }
}
