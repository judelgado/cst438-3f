package cst438hw3.domain;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class Country
{

    @Id
    private String code;
    private String name;
    private String code2;

    public Country()
    {
        code = "code";
        name = "name";
        code2 = "cd";
    }

    public Country(String code, String name, String code2)
    {
        this.code = code;
        this.name = name;
        this.code2 = code2;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode2()
    {
        return code2;
    }

    public void setCode2(String code2)
    {
        this.code2 = code2;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "Country [code=" + code + ", name=" + name + "]";
    }

}