package cst438hw3.service;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import cst438hw3.domain.City;
import cst438hw3.domain.CityInfo;
import cst438hw3.domain.CityRepository;
import cst438hw3.domain.Country;
import cst438hw3.domain.CountryRepository;
import cst438hw3.domain.TempAndTime;

@Service
public class CityService
{
    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private FanoutExchange fanout;

    public CityInfo getCityInfo(String cityName)
    {
        List<City> cities = cityRepository.findByName(cityName);

        // If there are no entries in the database, return null.
        if (cities.size() <= 0)
        {
            return null;
        }

        // Get the country and the city.
        City city = cities.get(0);
        Country country = countryRepository.findByCode(city.getCountryCode());

        // Load the data into the CityInfo
        TempAndTime tnt;
        try
        {
            tnt = weatherService.getTempAndTime(city.getName(), country.getCode2());
        } catch (Exception e)
        {
            return null;
        }

        return new CityInfo(city, country.getName(), tnt.getfahrenheit(), tnt.getTime());
    }

    public void requestReservation(String cityName, String level, String email)
    {
        String msg = "{\"cityName\": \"" + cityName + "\" \"level\": \"" + level + "\" \"email\": \"" + email + "\"}";
        System.out.println("Sending message:" + msg);
        rabbitTemplate.convertSendAndReceive(fanout.getName(), "", // routing key none.
                msg);
    }

}
