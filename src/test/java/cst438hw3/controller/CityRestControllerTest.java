package cst438hw3.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import cst438hw3.domain.*;
import cst438hw3.service.CityService;

import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(CityRestController.class)
public class CityRestControllerTest
{

    @MockBean
    private CityService cityService;

    @Autowired
    private MockMvc mvc;

    // This object will be magically initialized by the initFields method below.
    private JacksonTester<CityInfo> jsonCityInfo;

    @Before
    public void setup()
    {
        JacksonTester.initFields(this, new ObjectMapper());
    }

    @Test
    public void contextLoads()
    {
    }

    @Test
    public void getCityInfo() throws Exception
    {
        // expected Reservation to be returned
        CityInfo expected = new CityInfo(123, "bogota", "COL", "Colombia", "Cundinamarca", 40000000, 60.0, "01:30 AM");

        // stub out the Reservation Service class
        given(cityService.getCityInfo("bogota")).willReturn(expected);

        // when a city info
        MockHttpServletResponse response = mvc
                .perform(get("/api/cities/bogota").contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        // then return the expected city info.
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(jsonCityInfo.write(expected).getJson());
    }

    @Test
    public void getCityInfoNotFound() throws Exception
    {
        // stub out the Reservation Service class
        given(cityService.getCityInfo("not_found")).willReturn(null);

        // when requesting a city not found.
        MockHttpServletResponse response = mvc
                .perform(get("/api/cities/not_found").contentType(MediaType.APPLICATION_JSON)).andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

}
