package cst438hw3.service;

import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import cst438hw3.domain.*;

@SpringBootTest
public class CityServiceTest
{

    @MockBean
    private WeatherService weatherService;

    @Autowired
    private CityService cityService;

    @MockBean
    private CityRepository cityRepository;

    @MockBean
    private CountryRepository countryRepository;

    @Test
    public void contextLoads()
    {
    }

    @Test
    public void testCityFound() throws Exception
    {
        CityInfo testCityInfo = new CityInfo(123, "bogota", "COL", "Colombia", "Cundinamarca", 40000000, 60.0,
                "01:30 AM");

        City testCity = new City(123, "bogota", "COL", "Cundinamarca", 40000000);
        List<City> testCities = new ArrayList<City>();
        testCities.add(testCity);

        Country testCountry = new Country("COL", "Colombia", "CO");

        // Mocked the City and country to return their test models.
        given(cityRepository.findByName("bogota")).willReturn(testCities);
        given(countryRepository.findByCode("COL")).willReturn(testCountry);

        // Mock the weather service to return a test model set for 1 hour, and 30 mins.
        given(weatherService.getTempAndTime("bogota", "CO")).willReturn(new TempAndTime(288.706, 3600, 1800));

        // Call the cityService
        CityInfo actual = cityService.getCityInfo("bogota");

        // Verify that actual CityInfo is equal to the expected data.
        assertThat(actual).isEqualTo(testCityInfo);
    }

    @Test
    public void testCityNotFound()
    {
        // Mock an empty return of cities from the database.
        List<City> emptyCities = new ArrayList<City>();
        given(cityRepository.findByName("bad_city")).willReturn(emptyCities);

        // Call the cityService
        CityInfo actual = cityService.getCityInfo("bad_city");

        // Assert that if the database call is not found, the cityInfo is null.
        assertNull(actual);
    }

    @Test
    public void testWeatherNotFound()
    {
        City testCity = new City(123, "bogota", "COL", "Cundinamarca", 40000000);
        List<City> testCities = new ArrayList<City>();
        testCities.add(testCity);

        // Mock the city database call.
        given(cityRepository.findByName("bogota")).willReturn(testCities);

        // Mock the 404 not found exception thrown when the city is not found.
        given(weatherService.getTempAndTime("bogota", "CO")).willAnswer(invocation ->
        {
            throw new Exception("abc msg");
        });

        // Verify that the CityInfo is null when the city is not found.
        CityInfo actual = cityService.getCityInfo("bogota");
        assertNull(actual);
    }

    @Test
    public void testCityMultiple()
    {
        CityInfo testCityInfo = new CityInfo(123, "testCity", "TC1", "testCountry1", "testDistrict1", 10000000, 60.0,
                "01:30 AM");

        City testCity1 = new City(123, "testCity", "TC1", "testDistrict1", 10000000);
        City testCity2 = new City(456, "testCity", "TC2", "testDistrict2", 20000000);
        City testCity3 = new City(789, "testCity", "TC3", "testDistrict3", 30000000);
        List<City> testCities = new ArrayList<City>();
        testCities.add(testCity1);
        testCities.add(testCity2);
        testCities.add(testCity3);

        Country testCountry = new Country("TC1", "testCountry1", "T1");

        // Mocked the City and country to return their test models.
        given(cityRepository.findByName("testCity")).willReturn(testCities);
        given(countryRepository.findByCode("TC1")).willReturn(testCountry);

        // Mock the weather service to return a test model set for 1 hour, and 30 mins.
        given(weatherService.getTempAndTime("testCity", "T1")).willReturn(new TempAndTime(288.706, 3600, 1800));

        // Call the cityService
        CityInfo actual = cityService.getCityInfo("testCity");

        // Verify that actual CityInfo is equal to the expected data.
        assertThat(actual).isEqualTo(testCityInfo);
    }
}
